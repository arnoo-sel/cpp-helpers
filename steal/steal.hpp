// Attribute:
// - Capturing:
// > steal_attr(class_name, attribute_type, attibute_name);
//
// - Getting the pointer to member:
// > get_attr_ptr(class_name, attribute);

// Method:
// - Capturing:
// > steal_method(class_name, return_type, method_name[, arguments_types, ...]);
//
// - Getting the pointer to member:
// > get_method_ptr(class_name, method_name[, arguments_types, ...])

#define steal_attr(c, n, t)					\
  namespace steal {						\
    struct steal_attr_##c##n { typedef t c::*type; };		\
    template class stow_private<steal_attr_##c##n, &c::n>;	\
  }

#define get_attr_ptr(c, n) steal::stowed<steal::steal_attr_##c##n>::value

#define steal_method(c, n, t, ...)					\
  namespace steal {							\
    template<typename...As> struct steal_method_##c##n;			\
    template<> struct steal_method_##c##n<c, ##__VA_ARGS__> { typedef t ( c::*type)( __VA_ARGS__ ); }; \
    template class stow_private<steal_method_##c##n<c, ##__VA_ARGS__>, &c::n>; \
  }

#define steal_const_method(c, n, t, ...)				\
  namespace steal {							\
    template<typename...As> struct steal_const_method_##c##n;		\
    template<> struct steal_const_method_##c##n<c, ##__VA_ARGS__> { typedef t ( c::*type)( __VA_ARGS__ ) const; }; \
    template class stow_private<steal_const_method_##c##n<c, ##__VA_ARGS__>, &c::n>; \
  }

#define get_method_ptr(c, n, ...) steal::stowed<steal::steal_method_##c##n<c, ##__VA_ARGS__>>::value
#define get_const_method_ptr(c, n, ...) steal::stowed<steal::steal_const_method_##c##n<c, ##__VA_ARGS__>>::value

namespace steal {
  template <class Tag>
  struct stowed
  {
    static typename Tag::type value;
  };
  template <class Tag>
  typename Tag::type stowed<Tag>::value;
  
  template <class Tag, typename Tag::type x>
  struct stow_private
  {
    stow_private() { stowed<Tag>::value = x; }
    static stow_private instance;
  };
  template <class Tag, typename Tag::type x>
  stow_private<Tag,x> stow_private<Tag,x>::instance; 
}
